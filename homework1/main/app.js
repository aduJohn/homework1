function distance(array1,array2){
    if (Array.isArray(array1) && Array.isArray(array2)){
        let cleanedArray = (numbers) => numbers.filter((num,i)=>numbers.indexOf(num)===i)
        array1 = cleanedArray(array1)
        array2 = cleanedArray(array2)
        myDistance = 0
        array1.forEach((num)=>{
            if(!array2.includes(num)){
                myDistance++
            }
        })
        array2.forEach((num)=>{
            if(!array1.includes(num)){
                myDistance++
            }
        })
        return myDistance
    }else{
        throw new Error("InvalidType")
    }
}


module.exports.distance = distance